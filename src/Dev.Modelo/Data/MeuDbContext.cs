using Dev.Modelo.Models;
using Microsoft.EntityFrameworkCore;

namespace Dev.Modelo.Data
{
    public class MeuDbContext : DbContext 
    {
        public MeuDbContext(DbContextOptions<MeuDbContext> options): base(options)
        {
            
        }

        public DbSet<Aluno> Alunos { get; set; }
        
    }
}